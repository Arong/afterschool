<?php
	# 三元運算子 ? :
	# if else
	$a = 59;
	$b = 60;

	echo '<br>';
	echo '59分是否及格 ';
	echo $a-- >= 60 ? '及格' : '不及格';
	echo '<br>';
	echo '60分是否及格 ';
	echo $b >= 60 ? '及格' : '不及格';
	echo '<br>';

	$arr = null;
	echo $arr ?? 'hello';