<?php
// 駝峰式
# 1.變數命名採用首字母小寫駝峰式(camelCase)
# EX:
	echo PHP_EOL;
	$userName = 'Nick';
	echo 'User Name:' . $userName . PHP_EOL;
# 2.屬性命名採用首字母小寫駝峰式(camelCase)
# EX:

	$foodArr = ['rice', 'noodle'];
	print_r($foodArr);
	echo PHP_EOL;
# 3.function 命名採用首字母小寫駝峰式(camelCase)
# EX：

	function getUserName($userName) {
		return $userName;
	};
	echo getUserName("Arong") . PHP_EOL;
# 4.Class 命名採用首字母大寫駝峰式(StudlCaps)
# EX:

	class User {


// 蛇底式
# 通常用來判定這變數資料的來源是從資料庫來的
# 例如 User 資料表中的第一個User
		function getUser() {
		  	return [
		  		'user_id' => 1,
		  		'user_name' => 'Nick',
		  		'user_phone' => '0912345678',
		  		'user_birthdy' => '2020/06/17'
		  	];
		}
	}

	$userClass = new User;
	print_r($userClass->getUser());


