<?php
	$oddNumber = [];
	$evenNumber = [];
	for ($i =1; $i<=200; $i++) {
	    //echo $i%2 . PHP_EOL;
	    if ($i % 2 == 1) {
	        $oddNumber[] = $i;
	    } else {
	        $evenNumber[] = $i;
	    }
	}
	var_dump($oddNumber);
	var_dump($evenNumber);

	$sequence = [];
	for ($i=1;$i<=200;$i*=2) {
	    $sequence[] = $i;
	}
	var_dump($sequence);


	class MultiplicationTable {
		// 乘法表
		function MT(int $number) {
			for ($i=1;$i<10;$i++) {
				$result = $number * $i;
				echo $number . 'x' . $i . '=' . $result . '<br>';
			}
		}

		// 倍數
		function multiple(int $number, int $multiple) {
			for ($number; $number <= 200; $number+=$multiple) {
				echo $number . '<br>';
			}
		}

		// range number sum
		function rangeNumberSum($start, $end) {
			$result = 0;
			if ($start > $end) {
				for ($start; $start > $end; $start--) {
					$result += $start;
				}
			} else {
				for ($start; $start <= $end; $start++) {
					$result += $start;
				}
			}
			return $result;
		}
	}

	$multiplicationTable = new MultiplicationTable;
	switch (date('w')) {
		case '0':
			echo '今天週日<br>';
			echo '10的倍數：<br>';
			$multiplicationTable->multiple(10, 10);
			break;
		case '1':
			echo '今天週一<br>';
			echo '1的乘法表：<br>';
			$multiplicationTable->MT(1);
			echo '3的乘法表：<br>';
			$multiplicationTable->MT(3);
			echo '5的乘法表：<br>';
			$multiplicationTable->MT(5);
			break;
		case '2':
			echo '今天週二<br>';
			echo '2的乘法表：<br>';
			$multiplicationTable->MT(2);
			echo '4的乘法表：<br>';
			$multiplicationTable->MT(4);
			echo '6的乘法表：<br>';
			$multiplicationTable->MT(6);
			break;
		case '3':
			echo '今天週三<br>';
			echo '7的乘法表：<br>';
			$multiplicationTable->MT(7);
			echo '8的乘法表：<br>';
			$multiplicationTable->MT(8);
			echo '9的乘法表：<br>';
			$multiplicationTable->MT(9);
			break;
		case '4':
			echo '今天週四<br>';
			echo '1-100的總和：' . $multiplicationTable->rangeNumberSum(1, 100);
			break;
		case '5':
			echo '今天週五<br>';
			echo '5的倍數：<br>';
			$multiplicationTable->multiple(5, 5);
			break;
		case '6':
			echo '今天週六<br>';
			echo '40-0的總和：' . $multiplicationTable->rangeNumberSum(40, 0);
			break;
		default:
			echo '無法判讀您的時間。';
			break;
	}
