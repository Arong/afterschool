<?php
	echo '<br>一般陣列數組<br>';
	$fruits = [
		'apple',
		'banana',
		'cherry',
		'orange'
	];

	// foreach ($variable as $key => $value) {
	// 	# code...
	// }

	foreach ($fruits as $key => $fruit) {
	  echo $key . $fruit . '<br>';
	}

	echo '<br>鍵/值 陣列數組<br>';
	$fruits = [
		'a' => 'apple',
		'b' => 'banana',
		'c' => 'cherry',
		'o' => 'orange'
	];

	foreach ($fruits as $key => $fruit) {
		echo $key . '=>' . $fruit . '<br>';
	}