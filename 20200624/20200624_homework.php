<?php
	# 1.從1-200的迴圈中，
	# 顯示1-100中的奇數
	# 顯示101-200中的偶數，
	# 最後顯示出1-200中的等比數列(1, 2, 4, 8, 16, 32...依此類推)
	#
	# 2.用switch...case或是if...else...以及今天教的時間函式date()或是strtotime()和迴圈
	# 寫出時間是
	# 週一的時候顯示1、3、5的乘法表
	# 週二的時候顯示2、4、6的乘法表
	# 週三的時候顯示7、8、9的乘法表
	# 週四的時候顯示1-100的總和
	# 週五的時候顯示5的倍數
	# 週六的時候顯示40-0的總和
	# 週日的時候顯示使用call(呼叫) function()的方式顯示出10的倍數