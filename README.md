# AfterSchool

# git clone 教學
### 打開terminal 或 命令提示字元
### 輸入 cd C:\wamp64\www 或是 cd /Applications/MAMP/htdocs 進入Server目錄下的 www 或是 htdocs
### git clone https://gitlab.com/Arong/afterschool.git
### 打開網址輸入localhost/AfterSchool/20200618.php 或是 localhost/AfterSchool/20200618-variable.php
### 看到頁面表示成功

# git 基礎指令
### git clone: clone 一個 Repo (專案)
### git status: 目前Repo的狀態
### git commit -m "訊息": 提交當前修改過的檔案
### git push: 上傳到遠端 git
### git pull: 從遠端處下載更新到最新的repo 版本
