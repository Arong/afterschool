<?php
    echo '<br>';
    echo '今天是 ' . date('Y-m-d');
    // date('w') 是取得今天為星期幾 該日期函數所回傳的值 是字串 '0','1','2','3','4','5','6'

    switch (date('w')) {
        case '0':
            echo '星期日';
            break;
        case '1':
            echo '星期一';
            break;
        case '2':
            echo '星期二';
            break;
        case '3':
            echo '星期三';
            break;
        case '4':
            echo '星期四';
            break;
        case '5':
            echo '星期五';
            break;
        case '6':
            echo '星期六';
            break;
        default:
        // 可以把default想成是例外處理 當以上條件都不符合時才會跑default
            echo '未取得正確日期函數';
    }


    switch (date('w')) {
        case '0':
            # code...
            break;

        default:
            # code...
            break;
    }