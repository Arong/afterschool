<?php
	# do while 迴圈(loop) 無論如何都會先執行一次程式再做條件式的判斷
	echo '<br>列出數字 1 ~ 10<br>';

	$x = 1;

	do {
		// 先執行以下程式
		echo $x . "<br>";
		$x++;
		// $x++ 等於 $x=$x+1;
	} while ($x <= 10);