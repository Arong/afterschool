<!-- echo 只能印出string

integer、object、array、boolean

print_r() 能印出各樣的value(值)，但不會知道這value(值)屬於什麼型態，只有value

var_dump() 能印出各樣的value(值)，也能知道value(值)屬於何種型態

PHP_EOL 換行的意思 取代以下
1.windows OS相當於      echo "\r\n";
2.unix\linux OS相當於   echo "\n";
3.mac OS相當於          echo "\r";
<br> -->

<?php
	# String & Echo & 註解


	# 連接字串需要使用 .
	$name = 'Nick' . 'name';
	$talk = "I'm " . $name . PHP_EOL;
	$specialString = 'I\'m';

	// 這樣會顯示什麼呢？
	// echo $name . PHP_EOL;
	// var_dump($talk);
	// echo PHP_EOL;
	// echo $specialString . ' ' . $name . PHP_EOL;

	/*
		字串、顯示、註解
	 */

	# 數字介紹與處理

	# 變數型態
	$num0 = 10; // (integer)
	$num1 = '10'; // (string)
	// 這樣能否相加呢？
	// print_r($num0 + $num1);
	var_dump($num0 + $num1);
	# Integer
	# 整數
	$num = 10;
	// echo $num . PHP_EOL;

	# 減法運算
	$number = 15;
	$value = $number - 2 + 1;
	// echo $value . PHP_EOL;

	# 浮點數
	$number1 = 15.5;
	$value1 = $number1 - 2 + 1;
	echo $value1 . PHP_EOL;
	// var_dump($value1);

	# 負整數
	$thisIsInt2 = -37;
	echo $thisIsInt2 . PHP_EOL;
	var_dump($thisIsInt2);

	# Array
	# 陣列
	$test = array(
		'calss' => '網頁設計班',
		'社群剪輯班',
		'社群行銷班',
		'3D動畫班'
	);
	$courseData = [
		'calss' => '網頁設計班',
		'社群剪輯班',
		'社群行銷班',
		'3D動畫班'
	];
	// print_r($courseData);
	// var_dump($courseData);

	// $courseData = [
	// 	'calss0' => '網頁設計班',
	// 	'calss1' => '社群剪輯班',
	// 	'calss' => '社群行銷班',
	// 	'subItem' => [
	// 		'user',
	// 		'name',
	// 		'date'
	// 	],
	// ];
	// print_r($courseData['subItem'][1]);
	// var_dump($courseData);

	$arr = [
		"20200619" => "today",
		"tomorrow"
	];

	var_dump($arr);
	// 條件判斷式

	// boolean: true, false
	// var_dump(ture);
	# if false的條件有 false, null, [], '';
	#
	# && and
	# || or
	# <
	# >
	# ==
	# ===
	# <=
	# >=
	#  10 > 9
	#  10 == '10'
	#  10 === '10'
	if ([] and false && null && '') {
		echo 'True!';
	} elseif ('') {
		echo 'True!';
	} else {
		echo 'False!';
	}



	$score = 0;
	if ($score == 60) {
		echo '剛好及格！';
	} elseif ($score < 0 ) {
		echo '分數沒有負的！';
	} elseif ($score < 60 && $score > 0) {
		echo '不及格！';
	} elseif ($score > 60 && $score < 100) {
		echo '成績很好！';
	} else {
		echo '超過正常分數標準！';
	}